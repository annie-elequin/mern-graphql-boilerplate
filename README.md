## Put your MongoDB url in the location indicated in `index.js`.

You'll need a user in your db. You know the drill.

## Start client:

`yarn client`

## Start server:

`yarn server`

## START BOTH AT THE SAME TIME:

`yarn dev`
